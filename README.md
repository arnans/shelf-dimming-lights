# shelf dimming lights

A simple ESP8266 project to fade-in and fade-out LED strips installed on my DIY closet shelf.
- Left closet has 3 shelves. Each will fade-in/out in sequence (for extra coolness).
- on-off state of the LEDs are based on Two IR proximity sensors  