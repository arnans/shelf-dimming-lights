
// PWM Pins
// - D5 (GPIO 14) lower shelf
// - D6 (GPIO 12) middle shelf
// - D7 (GPIO 13) top shelf

const int LowerShelfPin = 14; 
const int MiddleShelfPin = 12; 
const int TopShelfPin = 13; 

// IR input Pins
// - D1 (GPIO5 ) left sensor
// - D2 (GPIO4 ) right sensor

const int leftSensor = 5; 
const int rightSensor = 4;

const int OFF = 0;
const int ON = 1;

int lightStatus = OFF;


const int OBJ_DETECTED = 1;
const int OBJ_UNDETECTED = 0;
int previousState = OBJ_UNDETECTED;

// -- natural fading calulations
// =============================
// The number of Steps between the output being on and off
const int pwmIntervals = 1024;

// The R value in the graph equation
float R;


// Initial setup
// ===============
int LowerShelfDuty = 0;
int MiddleShelfDuty = -300;
int TopShelfDuty = -600;


// =======================================
// Makes sure the PWM levels are in range
// Also, Mode = 1 adjusts the fade to be
// more natural looking.

int ValidifyLevel(int input, int mode) {
  int brightness;
  
  if (input <0) { brightness=0; }
  else if (input >1023) { brightness=1023;}
  else {brightness=input;}

  if (mode==0) {
      return(brightness);
  } else {
    // convert duty level to a natural fading brightness level
    return(pow (2, (brightness / R)) - 1);
  }
}

void setup() {
    // Calculate the R variable (only needs to be done once at setup)
    R = (pwmIntervals * log10(2))/(log10(1023));

    pinMode(leftSensor, INPUT);
    pinMode(rightSensor, INPUT);
}

void loop() {
  

  // ============================================
  //  Toggle light when hand swipe is detected
  // ============================================


  // when no hand detected
  if (!(digitalRead(leftSensor) ) && previousState == OBJ_DETECTED) {  

    if (lightStatus == OFF) {

      // increase the LED brightness
      for(int dutyCycle = 0; dutyCycle < 1600; dutyCycle+=2){   
        // changing the LED brightness with PWM
        analogWrite(LowerShelfPin, ValidifyLevel(dutyCycle + LowerShelfDuty, 1));
        analogWrite(MiddleShelfPin, ValidifyLevel(dutyCycle + MiddleShelfDuty, 1));
        analogWrite(TopShelfPin, ValidifyLevel(dutyCycle + TopShelfDuty, 1));
        delay(1);
      }
      lightStatus = ON;
    } else {

      // decrease the LED brightness
      for(int dutyCycle = 1600; dutyCycle >= 0; dutyCycle-=2){
        // changing the LED brightness with PWM
        analogWrite(LowerShelfPin, ValidifyLevel(dutyCycle + LowerShelfDuty, 0));
        analogWrite(MiddleShelfPin, ValidifyLevel(dutyCycle + MiddleShelfDuty, 0));
        analogWrite(TopShelfPin, ValidifyLevel(dutyCycle + TopShelfDuty, 0));
        delay(1);
      }
      lightStatus = OFF;  
    }

    previousState = OBJ_UNDETECTED; 
    delay(1000);

  }

  
  // if hand detected
  else if (digitalRead(leftSensor)) {
    previousState = OBJ_DETECTED;
  }





  
  /// ===============================================
  ///  Use this section when the sliding door is installed
  /// ===============================================
  
/*  
  // ============================================
  //  Turn on when door is open
  // ============================================
  
  if (!(digitalRead(leftSensor) || digitalRead(rightSensor)) && lightStatus == OFF) {  
//  if ((digitalRead(leftIR) == LOW) && (lightState = OFF)) {

    // increase the LED brightness
    for(int dutyCycle = 0; dutyCycle < 1600; dutyCycle+=2){   
      // changing the LED brightness with PWM
      analogWrite(LowerShelfPin, ValidifyLevel(dutyCycle + LowerShelfDuty, 1));
      analogWrite(MiddleShelfPin, ValidifyLevel(dutyCycle + MiddleShelfDuty, 1));
      analogWrite(TopShelfPin, ValidifyLevel(dutyCycle + TopShelfDuty, 1));
      delay(1);
    }
    lightStatus = ON;
    delay(1000);
  }

  // ============================================
  // Turn off when door is closed
  // ============================================
  
  else if ((digitalRead(leftSensor) || digitalRead(rightSensor)) && lightStatus == ON) {
    // decrease the LED brightness
    for(int dutyCycle = 1600; dutyCycle >= 0; dutyCycle-=2){
      // changing the LED brightness with PWM
      analogWrite(LowerShelfPin, ValidifyLevel(dutyCycle + LowerShelfDuty, 0));
      analogWrite(MiddleShelfPin, ValidifyLevel(dutyCycle + MiddleShelfDuty, 0));
      analogWrite(TopShelfPin, ValidifyLevel(dutyCycle + TopShelfDuty, 0));
      delay(1);
    }
  lightStatus = OFF;  
  delay(1000);
  }
*/

  delay(250);

  
}
